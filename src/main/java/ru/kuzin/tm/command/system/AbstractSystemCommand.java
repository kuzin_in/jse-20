package ru.kuzin.tm.command.system;

import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.command.AbstractCommand;
import ru.kuzin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}