package ru.kuzin.tm.api.model;

import ru.kuzin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
